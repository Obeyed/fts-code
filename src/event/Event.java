package event;

/**
 * This is the Event super class, symbolising atomic changes to the simulator state.
 */
public abstract class Event implements Comparable<Event> {
	private long eventTime;

	/**
	 * Constructs a Event
	 * @param eventTime the time the event should be executed
	 */
	public Event(long eventTime) {
		this.eventTime = eventTime;
	}

	/**
	 * The actions to be performed when the event is triggered
	 */
	public abstract void fire ();

	/**
	 * Compares this Event instance to another Event instance
	 * @param o the event to compare against
	 * @return 0 if the eventTimes are equal. 1 if this event's eventTime are greater than o's eventTime. -1 if neither
	 * previous conditions are met
	 */
	@Override
	public int compareTo(Event o) {
		if (getEventTime() == o.getEventTime()) {
			return 0;
		}
		else if (getEventTime() > o.getEventTime()) {
			return 1;
		}
		else {
			return -1;
		}
	}

	public long getEventTime() {
		return eventTime;
	}

	protected void setEventTime (long newEventTime) {
		eventTime = newEventTime;
	}
}
