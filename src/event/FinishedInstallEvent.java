package event;

import entity.Computer;

/**
 * A event that finishes the install of a computer
 */
public class FinishedInstallEvent extends Event {
    private Computer computer;

    /**
     * Constructs a FinishedInstallEvent
     * @param eventTime the time the event should be executed
     * @param computer reference to the computer on which to finish the install on
     */
    public FinishedInstallEvent(long eventTime, Computer computer) {
        super(eventTime);
        this.computer = computer;
    }

    /**
     * Calls finishReinstall() on the Computer instance
     */
    @Override
    public void fire() {
        computer.finishReinstall();
    }
}
