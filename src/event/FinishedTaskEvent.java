package event;

import entity.server.WorkServer;

/**
 * A event that finishes a task on a workserver
 */
public class FinishedTaskEvent extends Event {
	WorkServer workServer;

	/**
	 * Constructs a FinishedTaskEvent
	 * @param eventTime the time the event should be executed
	 * @param workServer the WorkServer that runs the task to be finished
	 */
	public FinishedTaskEvent(long eventTime, WorkServer workServer) {
		super(eventTime);

		this.workServer = workServer;
	}

	/**
	 * Calls finishTask() on workServer
	 */
	@Override
	public void fire() {
		workServer.finishTask();
	}
}
