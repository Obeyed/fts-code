package event;

import entity.Advert;
import entity.server.AdBoardServer;

/**
 * A event that removes a advert after its expiration time
 */
public class TearDownAdvertEvent extends Event {
    private AdBoardServer server;
    private Advert advert;

    /**
     * Constructs a TearDownAdvertEvent
     * @param eventTime the time the event should be executed
     * @param server the AdBoardServer to remove the advert from
     * @param advert the advert to remove
     */
    public TearDownAdvertEvent(long eventTime, AdBoardServer server, Advert advert) {
        super(eventTime);
        this.server = server;
        this.advert = advert;
    }

    /**
     * Calls tearDownAdvert(advert) on server
     */
    @Override
    public void fire() {
        server.tearDownAdvert(advert);
    }
}
