package event;

import entity.Task;
import simulator.Distributions;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.Arrays;
import java.util.Random;

/**
 * A event that creates a task and enqueus it
 */
public class NewTaskEvent extends Event {

	/**
	 * Constructs a NewTaskEvent
	 * @param eventTime the time the event should be executed
	 */
	public NewTaskEvent(long eventTime) {
		super(eventTime);
	}

	/**
	 * Creates a task with a variable execution time for a random virtual server and enqueues the task
	 * Creates a NewTaskEvent some time in future and enqueues it to ensure more Tasks get populated into the simulation
	 */
	@Override
	public void fire() {
		Random rand = new Random();
		int type = rand.nextInt((Integer)Simulator.instance().getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY));

		if (Simulator.instance().getCloud().isQueueingPossible(type)) {
			//TODO: Avoid use of literals (use the SimulationParameters system).
			long length = Distributions.getNewTaskRuntime(1); // 0.1 percent will be between 1000 and 1000000
			Task newTask = new Task(type, length);

			Simulator.instance().getCloud().queueTask(newTask);
		}

		//Create next instance of event
		//TODO: Clean up use of random
		long nextTaskTime = rand.nextInt(100) + Simulator.instance().getCurrentTime();
		this.setEventTime(nextTaskTime);
		Simulator.instance().queueEvent(null, this);
	}
}
