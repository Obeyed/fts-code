package event;

import entity.server.WorkServer;

/**
 * A event that causes a workserver to atttemp to work on a task
 */
public class ServerReQueryEvent extends Event {
    private WorkServer server;

    /**
     * Contructs a ServerReQueryEvent
     * @param eventTime the time the event should be executed
     * @param server the server that should attemp to work on a task
     */
    public ServerReQueryEvent(long eventTime, WorkServer server) {
        super(eventTime);
        this.server = server;
    }

    /**
     * Calls startNewTask() on server
     */
    @Override
    public void fire() {
        server.startNewTask();
    }
}
