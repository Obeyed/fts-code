package event;

import entity.Computer;

/**
 * A event that crashes a computer (DNSServer, Server)
 */
public class ComputerFailedEvent extends Event {
	Computer computer;

	/**
	 * Constructs a ComputerFailedEvent
	 * @param eventTime the time the event should be executed
	 * @param computer reference to the computer to crash
	 */
	public ComputerFailedEvent(long eventTime, Computer computer) {
		super(eventTime);

		this.computer = computer;
	}

	/**
	 * Calls breakdown() on the computer.
	 */
	@Override
	public void fire() {
		computer.breakdown();
	}
}
