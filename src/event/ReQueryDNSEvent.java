package event;

import entity.server.Server;

/**
 * A event that causes a server to try to contact a DNSServer in order to setup the server after an install
 */
public class ReQueryDNSEvent extends Event {
    private Server server;

    /**
     * Contructs a ReQueryDNSEvent
     * @param eventTime the time the event should be executed
     * @param server the server that should contact the DNSServer after an install
     */
    public ReQueryDNSEvent(long eventTime, Server server) {
        super(eventTime);
        this.server = server;
    }

    /**
     * Calls rebuildAfterInstall() on server
     */
    @Override
    public void fire() {
        server.rebuildAfterInstall();
    }
}
