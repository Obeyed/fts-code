package event;

import entity.Cloud;
import simulator.SimulationParameters;
import simulator.Simulator;

/**
 * A event that checks the health of a server
 */
public class CheckHealthEvent extends Event {
    private Cloud cloud;

    /**
     * Constructs a CheckHealthEvent
     * @param eventTime the time the event should be executed
     * @param cloud reference to the Cloud instance
     */
    public CheckHealthEvent(long eventTime, Cloud cloud) {
        super(eventTime);
        this.cloud = cloud;
    }

    /**
     * Calls checkServerHealth on the Cloud instance, creates a new CheckHealthEvent and enqueues it.
     */
    @Override
    public void fire() {
        cloud.checkServerHealth();

        long nextCheckTime = (Integer)Simulator.instance().getParams().get(SimulationParameters.CHECK_SERVER_INTERVAL_KEY)
                + Simulator.instance().getCurrentTime();
        CheckHealthEvent nextEvent = new CheckHealthEvent(nextCheckTime, cloud);
        Simulator.instance().queueEvent(null, nextEvent);
    }
}
