package simulator.datacollection;

import entity.Cloud;
import entity.Computer;
import entity.DNSServer;
import entity.server.AdBoardServer;
import entity.server.Server;
import entity.server.TaskQueueServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.*;

/**
 * This object collects and stores data about the
 * performance of a given cloud.
 */
public class DataCollector {
    private static final Logger log = LoggerFactory.getLogger(DataCollector.class);

    public static final long MILLIS = 1;
    public static final long SECONDS = 1000;
    public static final long MINUTES = 60000;
    public static final long HOURS = 3600000;
    public static final long DAYS = 86400000;

    private HashMap<Server,ServerStats> serverStatsMap;
    private CloudStats cloudStats = null;
    private long lastCollectionTime;

    /**
     * Instantiates a data collector.
     */
    public DataCollector() {
        serverStatsMap = new HashMap<>();
        lastCollectionTime = 0;
    }

    /**
     * Collects data from the given cloud, and bases these data
     * on the given time stamp.
     * @param cloud The cloud from which to collect data.
     * @param currentTime The time used to process the data.
     */
    public void collectData (Cloud cloud, long currentTime) {
        if (cloudStats == null) {
            cloudStats = new CloudStats((Integer)Simulator.instance().getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY));
        }

        collectServerData(cloud, currentTime);
        collectCloudHealthData(cloud, currentTime);

        lastCollectionTime = currentTime;
    }

    /**
     * Prints the collected data on the terminal.
     * @param unit The unit of time used for printing.
     *             One can use the built-in constants, fx. MINUTES.
     */
    public void printStats (long unit) {
        printCloudStats(unit);
        System.out.println();
        printServerStats(unit);
    }

    /**
     * Collects data concerning the cloud's servers, particularly
     * the time spent in various states.
     * @param cloud The cloud from which to collect data.
     * @param currentTime The time stamp used to calculate the time spent.
     */
    private void collectServerData(Cloud cloud, long currentTime) {
        Server[] servers = cloud.getServers();

        long interval = currentTime - lastCollectionTime;

        for (Server server : servers) {
            ServerStats stats = serverStatsMap.get(server);

            if (stats == null) {
                stats = createNewServerStats(server);
            }

            switch (server.getState()) {
                case Computer.RUNNING:
                    stats.incrementRunTime(interval);
                    break;
                case Computer.INSTALLING:
                    stats.incrementInstallTime(interval);
                    break;
                case Computer.FAILED:
                    stats.incrementBrokenTime(interval);
                    break;
            }
        }
    }

    /**
     * Collects data concerning the cloud's overall health, by
     * studying if entire sets of server types are down. If all
     * ad board servers are down, the entire cloud will be
     * considered down.
     * @param cloud The cloud from which to collect data.
     * @param currentTime The time stamp used to calculate the time spent.
     */
    private void collectCloudHealthData(Cloud cloud, long currentTime) {
        Server[] servers = cloud.getServers();
        DNSServer[] dnsServers = cloud.getDnsServers();
        int virtualServerCount = (Integer)Simulator.instance().getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY);

        // Sort the servers into lists.
        ArrayList<AdBoardServer> adBoardServers = new ArrayList<>();
        ArrayList<TaskQueueServer>[] taskQueueServers = new ArrayList[virtualServerCount];
        for (int virtualServerId = 0; virtualServerId < taskQueueServers.length; virtualServerId++) {
            taskQueueServers[virtualServerId] = new ArrayList<>();
        }

        for (Server server : servers) {
            if (server instanceof AdBoardServer) {
                adBoardServers.add((AdBoardServer)server);
            }
            else if (server instanceof TaskQueueServer) {
                taskQueueServers[((TaskQueueServer) server).getVirtualServerId()].add((TaskQueueServer)server);
            }
        }

        //If no server in a set is running, that set is down.
        boolean adBoardsDown = true;
        boolean[] taskQueuesDown = new boolean[virtualServerCount];
        Arrays.fill(taskQueuesDown, true);
        boolean dnsDown = true;

        for (AdBoardServer adBoardServer : adBoardServers) {
            if (adBoardServer.getState() == Computer.RUNNING) {
                adBoardsDown = false;
            }
        }

        for (int virtualServerId = 0; virtualServerId < virtualServerCount; virtualServerId++) {
            for (TaskQueueServer server : taskQueueServers[virtualServerId]) {
                if (server.getState() == Computer.RUNNING) {
                    taskQueuesDown[virtualServerId] = false;
                }
            }
        }

        for (DNSServer server : dnsServers) {
            if (server.getState() == Computer.RUNNING) {
                dnsDown = false;
            }
        }

        //If a set of servers is down, the cloud is down.
        boolean cloudDown = false;
        if (adBoardsDown || dnsDown) {
            cloudDown = true;
        }
        else {
            for (boolean taskQueueDown : taskQueuesDown) {
                if (taskQueueDown) {
                    cloudDown = true;
                }
            }
        }

        long interval = currentTime - lastCollectionTime;

        if (cloudDown) {
            cloudStats.incrementCloudDownTime(interval);
        }
        if (adBoardsDown) {
            cloudStats.incrementAdBoardDownTime(interval);
        }
        for (int virtualServerId = 0; virtualServerId < virtualServerCount; virtualServerId++) {
            if (taskQueuesDown[virtualServerId]) {
                cloudStats.incrementTaskQueueDownTime(interval, virtualServerId);
            }
        }
        if (dnsDown) {
            cloudStats.incrementDnsDownTime(interval);
        }
    }

    /**
     * Print the stats on cloud health collected by this object.
     * @param unit The unit of time being printed.
     */
    private void printCloudStats(long unit) {
        long totalTime = Simulator.instance().getCurrentTime();
        String unitName = getUnitName(unit);

        System.out.println("CLOUD STATS:");
        System.out.println("Time stats:");
        System.out.printf("\tTotal runtime:             %8.2f %s\n", (double) totalTime / unit, unitName);
        System.out.printf("\tCloud down time:           %8.2f %s\n", (double)cloudStats.getCloudDownTime()/unit, unitName);
        System.out.printf("\tAd board down time:        %8.2f %s\n", (double)cloudStats.getAdBoardDownTime()/unit, unitName);
        for (int virtualServerId = 0; virtualServerId < cloudStats.getTaskQueueTypeCount(); virtualServerId++) {
            System.out.printf("\tTask queue down time (%d):  %8.2f %s\n", virtualServerId+1, (double)cloudStats.getTaskQueueDownTime(virtualServerId)/unit, unitName);
        }
        System.out.printf("\tDNS down time:             %8.2f %s\n", (double)cloudStats.getDnsDownTime()/unit, unitName);

        System.out.println("Percent stats:");
        System.out.printf("\tTotal runtime:             %8.2f%%\n", (double) totalTime / totalTime * 100);
        System.out.printf("\tCloud down time:           %8.2f%%\n", (double)cloudStats.getCloudDownTime()/totalTime * 100);
        System.out.printf("\tAd board down time:        %8.2f%%\n", (double)cloudStats.getAdBoardDownTime()/totalTime * 100);
        for (int virtualServerId = 0; virtualServerId < cloudStats.getTaskQueueTypeCount(); virtualServerId++) {
            System.out.printf("\tTask queue down time (%d):  %8.2f%%\n", virtualServerId, (double)cloudStats.getTaskQueueDownTime(virtualServerId)/totalTime * 100);
        }
        System.out.printf("\tDNS down time:             %8.2f%%\n", (double)cloudStats.getDnsDownTime()/totalTime * 100);
    }

    /**
     * Print the stats on server states collected by this object.
     * @param unit The unit of time being printed.
     */
    private void printServerStats(long unit) {
        Iterator<ServerStats> statsIterator = serverStatsMap.values().iterator();
        int serverCount = 0;
        long sumTotalLifeTime = 0;
        long sumRunTime = 0;
        long sumInstallTime = 0;
        long sumBrokenTime = 0;
        int[] typesServerCount = new int[3];
        long[] typesSumTotalLifeTime = new long[3];
        long[] typesSumRunTime = new long[3];
        long[] typesSumInstallTime = new long[3];
        long[] typesSumBrokenTime = new long[3];

        Arrays.fill(typesServerCount, 0);
        Arrays.fill(typesSumTotalLifeTime, 0);
        Arrays.fill(typesSumRunTime, 0);
        Arrays.fill(typesSumInstallTime, 0);
        Arrays.fill(typesSumBrokenTime, 0);

        while (statsIterator.hasNext()) {
            ServerStats stats = statsIterator.next();

            serverCount++;
            sumTotalLifeTime += stats.getTotalLifeTime();
            sumRunTime += stats.getRunTime();
            sumInstallTime += stats.getInstallTime();
            sumBrokenTime += stats.getBrokenTime();

            int type = -1;
            switch (stats.getType()) {
                case "WorkServer":
                    type = 0;
                    break;
                case "AdBoardServer":
                    type = 1;
                    break;
                case "TaskQueueServer":
                    type = 2;
                    break;
                default:
                    log.warn("Unknown type identifier: {}", stats.getType());
            }

            if (type >= 0) {
                typesServerCount[type]++;
                typesSumTotalLifeTime[type] += stats.getTotalLifeTime();
                typesSumRunTime[type] += stats.getRunTime();
                typesSumInstallTime[type] += stats.getInstallTime();
                typesSumBrokenTime[type] += stats.getBrokenTime();
            }

        }

        String unitName = getUnitName(unit);

        // a.k.a. mean time to repair/return
        double meanDowntime = (double)(sumBrokenTime+sumInstallTime) / serverCount / unit;
        // a.k.a mean time to failure
        double meanUptime = (double)(sumRunTime) / serverCount / unit;

        System.out.println("SERVER STATS:");
        System.out.println("Overall:");
        System.out.printf("\tNo. of server instances:   %8d\n", serverCount);
        System.out.printf("\tAvg. time between failure: %8.2f %s\n", (double)sumTotalLifeTime / serverCount / unit, unitName);
        System.out.printf("\tAvg. running time (MTTF):  %8.2f %s\n", meanUptime, unitName);
        System.out.printf("\tAvg. install time:         %8.2f %s\n", (double)sumInstallTime / serverCount / unit, unitName);
        System.out.printf("\tAvg. broken time:          %8.2f %s\n", (double)sumBrokenTime / serverCount / unit, unitName);
        System.out.printf("\tAvg. repair time (MTTR):   %8.2f %s\n", meanDowntime, unitName);
        System.out.printf("\tAvailability:              %8.2f %%\n", (meanUptime/(meanUptime+meanDowntime))*100);

        String[] serverTypes = {"Work servers", "Ad boards", "Task queues"};

        for (int serverTypeIndex = 0; serverTypeIndex < serverTypes.length; serverTypeIndex++) {

            meanDowntime = (double)(typesSumBrokenTime[serverTypeIndex]+typesSumInstallTime[serverTypeIndex]) / typesServerCount[serverTypeIndex] / unit;
            meanUptime = (double)typesSumRunTime[serverTypeIndex] / typesServerCount[serverTypeIndex] / unit;

            System.out.println(serverTypes[serverTypeIndex] + ":");
            System.out.printf("\tNo. of server instances:   %8d\n", typesServerCount[serverTypeIndex]);
            System.out.printf("\tAvg. time between failure: %8.2f %s\n", (double)typesSumTotalLifeTime[serverTypeIndex] / typesServerCount[serverTypeIndex] / unit, unitName);
            System.out.printf("\tAvg. running time (MTTF):  %8.2f %s\n", (double)typesSumRunTime[serverTypeIndex] / typesServerCount[serverTypeIndex] / unit, unitName);
            System.out.printf("\tAvg. install time:         %8.2f %s\n", (double)typesSumInstallTime[serverTypeIndex] / typesServerCount[serverTypeIndex] / unit, unitName);
            System.out.printf("\tAvg. broken time:          %8.2f %s\n", (double)typesSumBrokenTime[serverTypeIndex] / typesServerCount[serverTypeIndex] / unit, unitName);
            System.out.printf("\tAvg. repair time (MTTR):   %8.2f %s\n", meanDowntime, unitName);
            System.out.printf("\tAvailability:              %8.2f %%\n", (meanUptime/(meanUptime+meanDowntime))*100);
        }
    }

    /**
     * Creates a new ServerStats object, and associates it with
     * the given server in the server stats map.
     * @param server The server for which to create the stats.
     * @return The stats created.
     */
    private ServerStats createNewServerStats(Server server) {
        ServerStats stats = new ServerStats(server.getClass().getSimpleName());
        serverStatsMap.put(server, stats);

        return stats;
    }

    /**
     * Gets the name of a unit based on the multiplier provided.
     * The unit name is determined from the relationship between
     * milliseconds and the multiplier. Thus, if the multiplier
     * is 60000 (the number of milliseconds in a minute), the string
     * "min." is returned.
     * @param multiplier The multiplier from which to get the unit name.
     * @return A textual representation of the determined unit.
     */
    private String getUnitName(long multiplier) {
        String unitName;

        switch (Long.toString(multiplier)) {
            case "1":
                unitName = "ms";
                break;
            case "1000":
                unitName = "s";
                break;
            case "60000":
                unitName = "min.";
                break;
            case "3600000":
                unitName = "hrs.";
                break;
            case "86400000":
                unitName = "days";
                break;
            default:
                unitName = "x " + multiplier + " ms";
        }

        return unitName;
    }
}
