package simulator.datacollection;

/**
 * Contains stats on how much time a server has spent in various states.
 */
public class ServerStats {
    private final String type;
    private long installTime;
    private long runTime;
    private long brokenTime;

    /**
     * Creates a new ServerStats object, with all time counts at 0.
     * @param type The type of server this object is associated with.
     */
    public ServerStats(String type) {
        this.type = type;
        installTime = 0;
        runTime = 0;
        brokenTime = 0;
    }

    public void incrementInstallTime (long amount) {
        installTime += amount;
    }

    public void incrementRunTime (long amount) {
        runTime += amount;
    }

    public void incrementBrokenTime (long amount) {
        brokenTime += amount;
    }

    public String getType() {
        return type;
    }

    public long getTotalLifeTime () {
        return installTime + runTime + brokenTime;
    }

    public long getInstallTime() {
        return installTime;
    }

    public long getRunTime() {
        return runTime;
    }

    public long getBrokenTime() {
        return brokenTime;
    }
}
