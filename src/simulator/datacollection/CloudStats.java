package simulator.datacollection;

import java.util.Arrays;

/**
 * Contains stats on how long various components in the cloud have been up.
 */
public class CloudStats {
	private long cloudDownTime;
	private long adBoardDownTime;
	private long[] taskQueuesDownTime;
	private long dnsDownTime;

	/**
	 * Creates a new CloudStats object, with all time counts at 0.
	 * @param virtualServerCount The number virtual servers in the cloud
	 *                           from which to collect stats.
	 */
	public CloudStats(int virtualServerCount) {
		cloudDownTime = 0;
		adBoardDownTime = 0;
		taskQueuesDownTime = new long[virtualServerCount];
		Arrays.fill(taskQueuesDownTime, 0);
		dnsDownTime = 0;
	}

	public long getCloudDownTime() {
		return cloudDownTime;
	}

	public long getAdBoardDownTime() {
		return adBoardDownTime;
	}

	public long getTaskQueueDownTime(int virtualServerId) {
		return taskQueuesDownTime[virtualServerId];
	}

	public int getTaskQueueTypeCount() {
		return taskQueuesDownTime.length;
	}

	public long getDnsDownTime() {
		return dnsDownTime;
	}

	public void incrementCloudDownTime (long amount) {
		cloudDownTime += amount;
	}

	public void incrementAdBoardDownTime(long amount) {
		adBoardDownTime += amount;
	}

	public void incrementTaskQueueDownTime (long amount, int virtualServerId) {
		taskQueuesDownTime[virtualServerId] += amount;
	}

	public void incrementDnsDownTime (long amount) {
		dnsDownTime += amount;
	}

}
