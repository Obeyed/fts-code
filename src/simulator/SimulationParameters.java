package simulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Contains the set of parameters for a simulation. Needs to be instantiated from a YAML file.
 */
public class SimulationParameters {
    /**
     * Logger instance
     */
    private static final Logger log = LoggerFactory.getLogger(SimulationParameters.class);
    /**
     * Property keys
     */
    public static final String SIMULATION_END_TIME_KEY = "SIMULATION_END_TIME";
    public static final String VIRTUAL_SERVER_COUNT_KEY = "VIRTUAL_SERVER_COUNT";
    public static final String SERVER_COUNT_KEY = "SERVER_COUNT";
    public static final String REDUNDANCY_DEGREE_KEY = "REDUNDANCY_DEGREE";
    public static final String ATTEMPT_CHANGE_INTERVAL_KEY = "ATTEMPT_CHANGE_INTERVAL";
    public static final String CHECK_SERVER_INTERVAL_KEY = "CHECK_SERVER_INTERVAL";
    public static final String ADVERT_EXPIRATION_TIME_KEY = "ADVERT_EXPIRATION_TIME";
    public static final String REQUERY_INTERVAL_KEY = "REQUERY_INTERVAL";
    public static final String INSTALL_TIME_KEY = "INSTALL_TIME";
    public static final String INITIAL_TASK_COUNT_KEY = "INITIAL_TASK_COUNT";
    public static final String ADVERT_POSTING_PROBABILITY_KEY = "ADVERT_POSTING_PROBABILITY";
    public static final String MAX_QUEUE_SIZE_KEY = "MAX_QUEUE_SIZE";
    public static final String SCOUTS_PR_SERVER_KEY = "SCOUTS_PR_SERVER";
    public static final String REQUERY_DNS_INTERVAL_KEY = "REQUERY_DNS_INTERVAL";

    /**
     * Hash map of keys/value
     */
    private HashMap<String,Object> propertyMap;

    /**
     * Construct instance from file
     * @param file YAML file
     */
    @SuppressWarnings("unchecked")
    public SimulationParameters(File file) {
        try {
            Yaml yaml = new Yaml();
            FileInputStream fis = new FileInputStream(file);
            propertyMap = (HashMap<String, Object>) yaml.load(fis);
            fis.close();
        }
        catch (IOException ex) {
            log.error("Error while accessing property file.");
            System.exit(1);
        }

    }

    /**
     * Get value for specific property key
     * @param propertyKey Key
     * @return Value object
     */
    public Object get(String propertyKey) {
        if(propertyMap.containsKey(propertyKey)){
            return propertyMap.get(propertyKey);
        }
        else{
            log.error("Tried to return value for unknown property key: {}.", propertyKey);
            return null;
        }
    }
}
