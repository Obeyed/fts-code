package simulator;

import org.apache.commons.math3.distribution.*;

import java.util.Random;

/**
 * Provides facilities to create random variables from various distributions.
 */
public final class Distributions {

	static Random random = new Random();
	static NormalDistribution nd = new NormalDistribution(25,50);

	private Distributions(){}

	/**
	 *
	 * @param perMille Per mille of numbers higher than 1000 generated
	 * @return A integer between 0 and 1,000,000.
	 */
	public static int getNewTaskRuntime(int perMille){
		int result;
		int decisionBoundary = 1000 - perMille;
		int decide = randomInt(1, 1000);
		if (decide >= decisionBoundary) {
			result = randomInt(1000, 1000000);
		}
		else {
			do {
				result = (int)nd.sample();
			} while(result < 0);
		}
		return result;
	}

	private static int randomInt(int min, int max){
		int randomNum = random.nextInt((max - min) + 1) + min;
		return randomNum;
	}
}
