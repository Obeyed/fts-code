package simulator;

import entity.Cloud;
import entity.Task;
import entity.server.WorkServer;
import event.Event;
import event.NewTaskEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.datacollection.DataCollector;

import java.io.File;
import java.util.*;

/**
 * A simulator, that sets up entities and events, runs the simulation, and reports on results.
 */
public class Simulator {
    /**
     * Logger instance
     */
    private static final Logger log = LoggerFactory.getLogger(Simulator.class);

    /**
     * The singleton instance of the simulator.
     */
    private static Simulator simulatorInstance;

    private SimulationParameters params;

    private DataCollector dataCollector;

    /**
     * Simulation time elapsed (from 0)
     */
	private long time;

    /**
     * Simulation end time
     */
	private final long endTime;

    /**
     * Queue with simulation events. Automatically sorts the events according to their time.
     */
	private PriorityQueue<Event> eventQueue;

    /**
     * Multi-map of next events related to each entity.
     */
    private HashMap<Object,ArrayList<Event>> entityEventMap;

    /**
     * The cloud (the simulator domain)
     */
    private Cloud cloud;

    /**
     * Constructor
     * @param configFile YAML file with configurations
     * @throws Exception Various exceptions not handled in code.
     */
	public Simulator(File configFile) throws Exception {
        params = new SimulationParameters(configFile);
        dataCollector = new DataCollector();
		time = 0;
		eventQueue = new PriorityQueue<>();
        entityEventMap = new HashMap<>();

        long endTimeTemp = 0;
        Object endTimeObj = params.get(SimulationParameters.SIMULATION_END_TIME_KEY);
        try {
            endTimeTemp = ((Integer) endTimeObj).longValue();
        }
        catch (ClassCastException ex) {
            try {
                endTimeTemp = (Long)params.get(SimulationParameters.SIMULATION_END_TIME_KEY);
            }
            catch (ClassCastException newEx) {
                log.error("Could not cast object: " + endTimeObj);
                System.exit(1);
            }
        }

        endTime = endTimeTemp;
	}

    /**
     * Initialises the simulated entities (the cloud and its members),
     * creates the first events, and puts the cloud to work.
     */
    public void initialiseSimulation () {
        cloud = new Cloud(params);
        addInitialTasks();

        NewTaskEvent event = new NewTaskEvent(getCurrentTime());
        queueEvent(null, event);

        for (WorkServer server : getCloud().getWorkServers()) {
            server.startNewTask();
        }
    }

    /**
     * Runs the simulation. An event is polled from the queue, th time
     * is set tot he time of that event, the data collector is invoked,
     * and the event is fired.
     */
	private void run() {
        int simulatedDays = 0;
        long startTimeMillis = new Date().getTime();

		while (time < endTime && !eventQueue.isEmpty()) {
			Event event = eventQueue.poll();
            time = event.getEventTime();

            dataCollector.collectData(cloud, time);

            event.fire();

            //Print message if simulation reaches a new day
            if (time / 86400000 > simulatedDays) {
                simulatedDays++;
                log.info("Day {} - {} ms.", simulatedDays, time);

                //Do "daily" garbage collection
                System.gc();
            }
		}

        double runTime = (new Date().getTime() - startTimeMillis) / 1000.0;

        //Do GC, and let the loggers flush
        try {
            System.gc();
            Thread.sleep(200);
        }
        catch (InterruptedException ex) {
            //Do nothing
        }

        System.out.printf("Simulation completed in %.3f seconds.\n", runTime);
        System.out.println();

        dataCollector.printStats(DataCollector.MINUTES);
    }

    /**
     * Creates a number of initial tasks, so the cloud doesn't start with nothing to do.
     */
    private void addInitialTasks () {
        int initialTaskCount = (Integer) params.get(SimulationParameters.INITIAL_TASK_COUNT_KEY);

        for (int i = 0; i < initialTaskCount; i++) {
            Random rand = new Random();
            int type = rand.nextInt((Integer)getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY));
            //TODO: Avoid use of literals (use the SimulationParameters system).
            long length = Distributions.getNewTaskRuntime(100); // 1 percent will be between 1000 and 1000000
            Task newTask = new Task(type, length);

            Simulator.instance().getCloud().queueTask(newTask);
        }
    }

    /**
     * Main method of the application.
     * @param args Input arguments. The name of a parameter file must be given.
     * @throws Exception Various exceptions not handled in code.
     */
	public static void main(String[] args) throws Exception {
        setupLogger();

        //Exit when config file is not found
        if (!checkConfigFileExists(args)) System.exit(1);

        File configFile = new File(args[0]);
		simulatorInstance = new Simulator(configFile);
        simulatorInstance.initialiseSimulation();
        simulatorInstance.run();
	}

    /**
     * Sets up the logger to show more concise messages, with an added time stamp.
     * These settings are applied throughout the application.
     */
    private static void setupLogger () {
        System.setProperty("org.slf4j.simpleLogger.showDateTime", "true");
        System.setProperty("org.slf4j.simpleLogger.dateTimeFormat", "yyyy-MM-dd hh:mm:ss.SSS");
        System.setProperty("org.slf4j.simpleLogger.showThreadName", "false");
        System.setProperty("org.slf4j.simpleLogger.showShortLogName", "true");
    }

    /**
     * Confirms if configurations file is specified and if it exists
     * @param args An array that should contain a file name for the configuration
     *             file in its first argument.
     * @return false is file is not specified or file does not exist, otherwise true
     */
    private static boolean checkConfigFileExists(String[] args) {
        //Check config file
        if (args.length < 1) {
            System.out.println("No configuration file indicated.");
            return false;
        }

        File configFile = new File(args[0]);

        if (!configFile.exists()) {
            System.out.println("Configuration file " + configFile.getName() + " not found.");
            return false;
        }
        return true;
    }

    /**
     * Puts an event into the event queue. It also associates the event with
     * the given object in the entity-event map. As this map is a multi-map,
     * it will not overwrite other events already associated with this
     * object.
     * @param entity The object closest related with that event. May be null.
     * @param event The event to queue.
     */
    public void queueEvent (Object entity, Event event) {
        eventQueue.offer(event);

        if (entity != null) {
            ArrayList<Event> entityEvents = entityEventMap.get(entity);
            if (entityEvents == null) {
                entityEvents = new ArrayList<Event>();
                entityEventMap.put(entity, entityEvents);
            }

            entityEvents.add(event);
        }

    }

    /**
     * Gets the singleton instance of the simulator.
     * @return The instance.
     */
    public static Simulator instance() {
        return simulatorInstance;
    }

    public long getCurrentTime() {
        return time;
    }

    public Cloud getCloud() {
        return cloud;
    }

    public SimulationParameters getParams() {
        return params;
    }


    /**
     * Removes the queued events for a particular entity.
     * @param entity The entity to remove queued event for.
     */
    public void unqueueEntitysNextEvents(Object entity) {
        ArrayList<Event> entityEvents = entityEventMap.get(entity);

        if (entityEvents != null) {
            for (Event event : entityEvents) {
                eventQueue.remove(event);
            }
        }
    }

    /**
     * Removes an entity, and all events associated with it
     * from the entity-event map. This does not change the
     * event queue.
     * @param entity
     */
    public void removeEntityFromMap (Object entity) {
        if (entity != null) {
            entityEventMap.remove(entity);
        }
    }
}
