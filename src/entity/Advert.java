package entity;

/**
 * Advert for an virtual server. Adverts expires after some time
 */
public class Advert {
	private int virtualServerId;
	private long postTime;

	/**
	 * Constructs an advert
	 * @param virtualServerId the virtual server which the advert is to be created for
	 * @param postTime the current time
	 */
	public Advert(int virtualServerId, long postTime) {
		this.virtualServerId = virtualServerId;
		this.postTime = postTime;
	}

	public int getVirtualServerId() {
		return virtualServerId;
	}
}
