package entity;

/**
 * This interface contains states for all Servers and is implemented by all Servers
 */
public interface Computer {

    public static final int RUNNING = 0;
    public static final int INSTALLING = 1;
    public static final int FAILED = 2;

    public void breakdown ();

    public void finishReinstall ();

    public int getState ();

    public long getUptime ();
}
