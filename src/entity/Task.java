package entity;

/**
 * A task to be used by a WorkServer
 */
public class Task {
	private int type;
	private long remainingRuntime;

	/**
	 * Constructs a Task
	 * @param type Type of the task
	 * @param remainingRuntime Remaining time the task have to run
	 */
	public Task(int type, long remainingRuntime) {
		this.type = type;
		this.remainingRuntime = remainingRuntime;
	}

	public int getType() {
		return type;
	}

	public long getRemainingRuntime() {
		return remainingRuntime;
	}

	public void setRemainingRuntime(long remainingRuntime) {
		this.remainingRuntime = remainingRuntime;
	}
}
