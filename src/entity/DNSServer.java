package entity;

import entity.server.AdBoardServer;
import entity.server.Server;
import entity.server.TaskQueueServer;
import event.ComputerFailedEvent;
import event.FinishedInstallEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * A DNS server, for handling references to regular servers, allowing them interact.
 */
public class DNSServer implements Computer {
    private static final Logger log = LoggerFactory.getLogger(DNSServer.class);

    private final int REDUNDANCY_DEGREE;

    private int state;
    private long uptime;

    private LinkedList<TaskQueueServer>[] taskQueueServerLists;
    private LinkedList<AdBoardServer> adBoardServerList;
    private DNSServer[] dnsServers;

    /**
     * Create a DNS server with references to the task queue servers, and the ad board servers.
     * @param redundancyDegree The number of redundant servers to use for each role.
     *                         E.g. a degree of 3, results in three redundant ad board servers,
     *                         and three task queue servers for each virtual server.
     * @param taskQueueServerLists List of TaskQueueServers in the cloud
     * @param adBoardServerList List of AdBoardServers in the cloud
     * @param dnsServers List of DNSServers in the cloud
     */
    public DNSServer (int redundancyDegree, LinkedList<TaskQueueServer>[] taskQueueServerLists,
                      LinkedList<AdBoardServer> adBoardServerList, DNSServer[] dnsServers) {

        REDUNDANCY_DEGREE = redundancyDegree;
        this.state = RUNNING;

        //TODO: Refine random
        Random rand = new Random();
        long timeToBreakdown;
        do {
            timeToBreakdown = rand.nextInt(7776000) * 1000L - uptime;
        } while (timeToBreakdown < 0);
        long timeOfBreakdown = timeToBreakdown + Simulator.instance().getCurrentTime();

        ComputerFailedEvent event = new ComputerFailedEvent(timeOfBreakdown, this);
        Simulator.instance().queueEvent(this, event);

        this.taskQueueServerLists = taskQueueServerLists;
        this.adBoardServerList = adBoardServerList;
        this.dnsServers = dnsServers;

    }

    /**
     * Returns available TaskQueueServers and replaces the crashed ones with the calling server
     * @param virtualServerId The virtual server to return TaskQueueServers for
     * @param callingServer The server asking for TaskQueueServers
     * @return The list of running TaskQueueServers if the DNSServer is available. null either if:
     * - the calling server should be reinstalled to replace a TaskQueueServer
     * - the DNSServer(s) is not running
     * - no TaskQueueServers are available
     */
    public TaskQueueServer[] getTaskQueues(int virtualServerId, Server callingServer) {
        if (state == RUNNING) {
            if (isServerListHealthy(taskQueueServerLists[virtualServerId])) {
                return taskQueueServerLists[virtualServerId].toArray(new TaskQueueServer[taskQueueServerLists[virtualServerId].size()]);
            }
            else {
                if (callingServer != null) {
                    TaskQueueServer newTaskQueueServer = callingServer.reinstallAsTaskQueue(callingServer.getUptime(), virtualServerId);

                    for (DNSServer server : dnsServers) {
                        server.addNewTaskQueueServer(newTaskQueueServer);
                    }

                    return null;
                }
                else {
                    return (taskQueueServerLists[virtualServerId].isEmpty()) ?
                            null : taskQueueServerLists[virtualServerId].toArray(new TaskQueueServer[taskQueueServerLists[virtualServerId].size()]);
                }
            }
        }
        else {
            return null;
        }
    }

    /**
     * Returns available AdBoardServers and replaces the crashed ones with the calling server
     * @param callingServer The server asking for AdBoardServers
     * @return The list of running AdBoardServers if the DNSServer is available. null either if:
     * - the calling server should be reinstalled to replace a AdBoardServer
     * - the DNSServer(s) is not running
     * - no AdBoardServers are available
     */
    public AdBoardServer[] getAdBoards(Server callingServer) {
        if (state == RUNNING) {
            if (isServerListHealthy(adBoardServerList)) {
                return adBoardServerList.toArray(new AdBoardServer[adBoardServerList.size()]);
            }
            else {
                if (callingServer != null) {
                    AdBoardServer newAdBoardServer = callingServer.reinstallAsAdBoard(callingServer.getUptime());

                    for (DNSServer server : dnsServers) {
                        server.addNewAdBoardServer(newAdBoardServer);
                    }

                    return null;
                }
                else {
                    return (adBoardServerList.isEmpty()) ?
                            null : adBoardServerList.toArray(new AdBoardServer[adBoardServerList.size()]);
                }
            }
        }
        else {
            return null;
        }
    }

    /**
     * Adds a TaskQueueServer to it's virtual server's list of TaskQueueServers
     * @param server The TaskQueueServer to add it's virtual server's list of TaskQueueServers
     */
    public void addNewTaskQueueServer (TaskQueueServer server) {
        taskQueueServerLists[server.getVirtualServerId()].add(server);
    }

    /**
     * Adds a AdBoardServer to the list AdBoardServers
     * @param server The AdBoardServer to add to the list of AdBoardServers
     */
    public void addNewAdBoardServer (AdBoardServer server) {
        adBoardServerList.add(server);
    }

    /**
     * Removes an TaskQueueServer or AdBoardServer from the TaskQueueServer lists or from the AdBoardServer list
     * @param server The TaskQueueServer or AdBoardServer to remove
     */
    public void removeServer (Server server) {
        if (server instanceof TaskQueueServer) {
            for (LinkedList<TaskQueueServer> taskQueueServerList : taskQueueServerLists) {
                boolean removalSuccess = taskQueueServerList.remove(server);
                if (removalSuccess) {
                    break;
                }
            }
        }
        else if (server instanceof AdBoardServer) {
            adBoardServerList.remove(server);
        }
        else {
            log.warn("Tried to remove server of invalid type: {}", server.toString());
        }
    }

    /**
     * Method that begins a reinstall after a crash. The state is set to INSTALLING and an event to finish the
     * installation is created and enqueued. A event to crash the server is created and enqueued.
     */
    public void reinstall () {
        state = INSTALLING;
        uptime = 0;

        long timeOfReinstall = (Integer)Simulator.instance().getParams().get(SimulationParameters.INSTALL_TIME_KEY)
                + Simulator.instance().getCurrentTime();
        FinishedInstallEvent finishedInstallEvent = new FinishedInstallEvent(timeOfReinstall, this);
        Simulator.instance().queueEvent(this, finishedInstallEvent);

        //TODO: Refine random
        Random rand = new Random();
        long timeOfBreakdown = rand.nextInt(7776000) * 1000L + Simulator.instance().getCurrentTime();

        ComputerFailedEvent computerFailedEvent = new ComputerFailedEvent(timeOfBreakdown, this);
        Simulator.instance().queueEvent(this, computerFailedEvent);
    }

    /**
     * Method to be called when the installation is finished.
     * Recreates the TaskQueueServer lists and the AdBoardServer list.
     */
    @Override
    public void finishReinstall() {
        state = RUNNING;

        Server[] servers = Simulator.instance().getCloud().getServers();

        for (Server server : servers) {
            if (server instanceof TaskQueueServer) {
                taskQueueServerLists[((TaskQueueServer) server).getVirtualServerId()].add((TaskQueueServer)server);
            }
            else if (server instanceof AdBoardServer) {
                adBoardServerList.add((AdBoardServer)server);
            }
        }

        log.info("{}: {} ({}) reinstalled.", Simulator.instance().getCurrentTime(), this.getClass().getSimpleName(), this.hashCode());
    }

    /**
     * Method to be called when the DNSServer fails. Currently this means that the DNSServer loses track of which
     * type of Server a Server is.
     * Sets state to FAILED and clears the AdBoardServer list and TaskQueueServer lists
     */
    @Override
    public void breakdown() {
        state = FAILED;

        Simulator.instance().unqueueEntitysNextEvents(this);

        // DNS loses track of which servers solve which roles.
        adBoardServerList.clear();
        for (LinkedList<TaskQueueServer> list : taskQueueServerLists) {
            list.clear();
        }

        log.info("{}: {} ({}) broke down.", Simulator.instance().getCurrentTime(), this.getClass().getSimpleName(), this.hashCode());
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public long getUptime() {
        return uptime;
    }

    /**
     * Removes the FAILED servers and checks whether there are enough redundant servers
     * @param serverList A TaskQueueServer list for a virtual or a AdBoardServer list to be checked for FAILED servers
     * @return true if none of the servers in serverList is FAILED. Otherwise returns false
     */
    private boolean isServerListHealthy(List<? extends Server> serverList) {
        for (Server server : serverList) {
            if (server.getState() == FAILED) {
                for (DNSServer dnsServer : dnsServers) {
                    dnsServer.removeServer(server);
                }
            }
        }

        if (serverList.size() < REDUNDANCY_DEGREE) {
            return false;
        }
        else {
            return true;
        }
    }
}
