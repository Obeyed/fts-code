package entity;

import entity.server.AdBoardServer;
import entity.server.Server;
import entity.server.TaskQueueServer;
import entity.server.WorkServer;
import event.CheckHealthEvent;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

/**
 * A cloud service, consisting of several servers handling requests
 */
public class Cloud {
    private ArrayList<Server> servers;
    private DNSServer[] dnsServers;

    /**
     * Creates a new cloud, based on the given simulation parameters.
     * @param simulationParameters Holds the parameters used to create the cloud.
     */
    public Cloud(SimulationParameters simulationParameters) {
        int redundancyDegree = (Integer)simulationParameters.get(SimulationParameters.REDUNDANCY_DEGREE_KEY);
        int virtualServerCount = (Integer)simulationParameters.get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY);

        servers = new ArrayList<Server>();
        dnsServers = new DNSServer[redundancyDegree];

        Random rand = new Random();

        //Create Task queue servers
        LinkedList<TaskQueueServer>[] taskQueueServerLists = new LinkedList[virtualServerCount];
        int virtualServerId = 0;
        for (int i = 0; i < taskQueueServerLists.length; i++) {
            taskQueueServerLists[i] = new LinkedList<TaskQueueServer>();

            for (int j = 0 ; j < redundancyDegree ; j++) {
                //TODO: Refine use of random
                TaskQueueServer taskQueue = new TaskQueueServer(Computer.RUNNING, rand.nextInt(2592000) * 1000L, dnsServers, virtualServerId);
                taskQueueServerLists[i].add(taskQueue);
                servers.add(taskQueue);
            }
            virtualServerId++;
        }

        //Create adboard servers
        LinkedList<AdBoardServer> adBoardList = new LinkedList<AdBoardServer>();
        for (int i = 0 ; i < redundancyDegree ; i++) {
            //TODO: Refine use of random
            AdBoardServer adBoardServer = new AdBoardServer(Computer.RUNNING, rand.nextInt(2592000) * 1000L, dnsServers);
            adBoardList.add(adBoardServer);
            servers.add(adBoardServer);

        }

        //Create work servers
        int totalServerCount = (Integer)simulationParameters.get(SimulationParameters.SERVER_COUNT_KEY);
        virtualServerId = 0;
        for (int serverIndex = servers.size(); serverIndex < totalServerCount; serverIndex++) {
            //TODO: Refine use of random
            WorkServer workServer = new WorkServer(Computer.RUNNING, rand.nextInt(2592000) * 1000L, dnsServers, virtualServerId);
            servers.add(workServer);
            virtualServerId = (virtualServerId + 1) % virtualServerCount;
        }

        //Create DNS servers
        for (int dnsServerIndex = 0; dnsServerIndex < redundancyDegree; dnsServerIndex++) {
            LinkedList<AdBoardServer> clonedAdBoardList = (LinkedList<AdBoardServer>)adBoardList.clone();
            LinkedList<TaskQueueServer>[] clonedTaskQueueLists = new LinkedList[taskQueueServerLists.length];
            for (int listIndex = 0; listIndex < clonedTaskQueueLists.length; listIndex++) {
                clonedTaskQueueLists[listIndex] = (LinkedList<TaskQueueServer>)taskQueueServerLists[listIndex].clone();
            }

            dnsServers[dnsServerIndex] = new DNSServer(redundancyDegree, clonedTaskQueueLists, clonedAdBoardList, dnsServers);
        }

        //Create first CheckHealthEvent
        CheckHealthEvent event = new CheckHealthEvent(0, this);
        Simulator.instance().queueEvent(null, event);
    }

    /**
     * Queues a new task in the task queues associated with the virtual server
     * indicated in the task. If it is not possible to access a queue, the
     * request fails.
     * @param task The task to queue.
     */
    public void queueTask(Task task) {
        DNSServer dnsServer = getWorkingDNSServer(dnsServers);

        if (dnsServer != null) {
            TaskQueueServer[] taskQueueServers = dnsServer.getTaskQueues(task.getType(), null);

            if (taskQueueServers != null) {
                for (TaskQueueServer server : taskQueueServers) {
                    if (server.getState() == Computer.RUNNING) {
                        server.queueTask(task);
                    }
                }
            }
            else {
                //Cloud can do nothing, task is lost.
            }
        }
    }

    /**
     * Checks if the queues of the given virtual server are full.
     * @param virtualServerId The ID of the virtual server to check.
     * @return true if the queue has room for new tasks. False if
     * there is no room for new tasks, or the cloud could not access
     * the relevant task queues.
     */
    public boolean isQueueingPossible(int virtualServerId) {
        DNSServer dnsServer = getWorkingDNSServer(dnsServers);

        if (dnsServer != null) {
            TaskQueueServer[] taskQueueServers = dnsServer.getTaskQueues(virtualServerId, null);

            if (taskQueueServers != null) {
                for (TaskQueueServer server : taskQueueServers) {
                    if (server.getState() == Computer.RUNNING) {
                        return !server.isQueueFull();
                    }
                }
            }
        }

        //Queue is unavailable
        return false;
    }

    /**
     * Removes the given oldServer from the cloud server list, and adds the given
     * newServer. The new server may not be placed at the same location as the
     * old server in the list.
     * @param oldServer The server to remove.
     * @param newServer The server to add.
     */
    public void replaceServerInstance (Server oldServer, Server newServer) {
        servers.remove(oldServer);
        servers.add(newServer);
    }

    /**
     * Checks all servers known to the cloud for failures. If a server has failed,
     * is is reinstalled as work server.
     */
    public void checkServerHealth () {
        ArrayList<Server> failedServerList = new ArrayList<Server>();

        //Find all servers in the failed state.
        for (Server server : servers) {
            if (server.getState() == Computer.FAILED) {

                failedServerList.add(server);
            }
        }

        //Reinstall failed servers as work servers.
        for (Server failedServer : failedServerList) {
            DNSServer dnsServer = getWorkingDNSServer(dnsServers);

            boolean installStarted = false;

            if (dnsServer != null) {
                AdBoardServer[] adBoardServers = dnsServer.getAdBoards(null);

                //Get an ad from the ad board, and reinstall based on that
                if (adBoardServers != null) {
                    for (AdBoardServer adBoardServer : adBoardServers) {
                        if (adBoardServer.getState() == Computer.RUNNING) {
                            Advert advert = adBoardServer.getAdvert();
                            if (advert != null) {
                                int virtualServerId = advert.getVirtualServerId();
                                failedServer.reinstallAsWorker(0, virtualServerId);
                                installStarted = true;
                            }
                            break;
                        }
                    }
                }
            }

            //Unable to get an ad, reinstall to a random virtual server
            if (!installStarted) {
                Random rand = new Random();
                int virtualServerId = rand.nextInt((Integer) Simulator.instance().getParams()
                        .get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY));
                failedServer.reinstallAsWorker(0, virtualServerId);
            }
        }
    }

    /**
     * Get an array of all servers known to the cloud.
     * @return An array of servers.
     */
    public Server[] getServers () {
        return servers.toArray(new Server[servers.size()]);
    }

    /**
     * Get an array of all work servers known to the cloud.
     * @return An array of work servers.
     */
    public WorkServer[] getWorkServers () {
        ArrayList<WorkServer> workServerList = new ArrayList<WorkServer>();

        for (Server server : servers) {
            if (server instanceof WorkServer) {
                workServerList.add((WorkServer)server);
            }
        }

        return workServerList.toArray(new WorkServer[workServerList.size()]);
    }

    /**
     * Get an array of the DNS servers.
     * @return An array of DNS servers.
     */
    public DNSServer[] getDnsServers () {
        return dnsServers.clone();
    }

    /**
     * Gets a DNS server in the RUNNING state.
     * @param dnsServers The array of DNS servers to search through.
     * @return A running DNS server, or null if no such server was found.
     */
    public static DNSServer getWorkingDNSServer (DNSServer[] dnsServers) {
        DNSServer resultServer = null;

        for (DNSServer dnsServer : dnsServers) {
            if (dnsServer.getState() == Computer.RUNNING) {
                resultServer = dnsServer;
            }
            else if (dnsServer.getState() == Computer.FAILED) {
                dnsServer.reinstall();
            }
        }

        return resultServer;
    }
}
