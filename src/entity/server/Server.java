package entity.server;

import entity.Cloud;
import entity.DNSServer;
import entity.Computer;
import event.FinishedInstallEvent;
import event.ComputerFailedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.Random;

/**
 * Parent class of all physical servers in the system.
 */
public abstract class Server implements Computer {
    private static final Logger log = LoggerFactory.getLogger(Server.class);

    private int state;
    private long uptime;
    protected DNSServer[] dnsServerList;

    /**
     * Constructor for Server
     * @param state State of the server at creation. See Computer for valid status
     * @param uptime Uptime of the server at creation.
     * @param dnsServerList List of DNSServer that will be used for contacting TaskQueues and Adboards
     */
    public Server(int state, long uptime, DNSServer[] dnsServerList) {
        this.state = state;
        this.uptime = uptime;
        this.dnsServerList = dnsServerList;

        // create event for crashing the server and enqueue it on the event queue
        //TODO: Refine use of random.
        Random rand = new Random();
        long timeToBreakdown;
        do {
            timeToBreakdown = rand.nextInt(2592000) * 1000L - uptime;
        } while (timeToBreakdown < 0);
        long timeOfBreakdown = timeToBreakdown + Simulator.instance().getCurrentTime();
        ComputerFailedEvent failedEvent = new ComputerFailedEvent(timeOfBreakdown, this);
        Simulator.instance().queueEvent(this, failedEvent);

        // if installing at creation add event for finishing the install and enqueue it on the event queue
        if (this.state == Computer.INSTALLING) {
            long installFinishTime = (Integer) Simulator.instance().getParams().get(SimulationParameters.INSTALL_TIME_KEY) +
                    Simulator.instance().getCurrentTime();
            FinishedInstallEvent installedEvent = new FinishedInstallEvent(installFinishTime, this);
            Simulator.instance().queueEvent(this, installedEvent);
        }
    }

    @Override
    public int getState() {
        return state;
    }

    /**
     * Reinstalls a server to be a WorkServer. The previous server instance (in the Cloud) is replaced by a new
     * WorkServer instance. The previous servers events are deleted.
     * @param uptime Uptime of the server that is reinstalled.
     * @param virtualServerId The virtual server the workServer is assigned to
     * @return instance of a new WorkServer in state INSTALLING
     */
    public WorkServer reinstallAsWorker (long uptime, int virtualServerId) {
        WorkServer newServer = new WorkServer(Computer.INSTALLING, uptime, dnsServerList, virtualServerId);

        Simulator.instance().getCloud().replaceServerInstance(this, newServer);
        Simulator.instance().unqueueEntitysNextEvents(this);
        Simulator.instance().removeEntityFromMap(this);

        return newServer;
    }

    /**
     * Reinstalls a server to be a AdBoardServer. The previous server instance (in the Cloud) is replaced by a new
     * AdBoardServer instance. The previous servers events are deleted.
     * @param uptime Uptime of the server that is reinstalled.
     * @return instance of a new AdBoardServer in state INSTALLING
     */
    public AdBoardServer reinstallAsAdBoard(long uptime) {
        AdBoardServer newServer = new AdBoardServer(Computer.INSTALLING, uptime, dnsServerList);

        Simulator.instance().getCloud().replaceServerInstance(this, newServer);
        Simulator.instance().unqueueEntitysNextEvents(this);
        Simulator.instance().removeEntityFromMap(this);

        log.info("{}: {} ({}) reinstalled as ad board.", Simulator.instance().getCurrentTime(),
                this.getClass().getSimpleName(), this.hashCode());

        return newServer;
    }

    /**
     * Reinstalls a server to be a TaskQueueServer. The previous server instance (in the Cloud) is replaced by a new
     * TaskQueueServer instance. The previous servers events are deleted.
     * @param uptime Uptime of the server that is reinstalled.
     * @param virtualServerId The virtual server the TaskQueue is assigned to
     * @return instance of a new TaskQueueServer in state INSTALLING
     */
    public TaskQueueServer reinstallAsTaskQueue (long uptime, int virtualServerId) {
        TaskQueueServer newServer = new TaskQueueServer(Computer.INSTALLING, uptime, dnsServerList, virtualServerId);

        Simulator.instance().getCloud().replaceServerInstance(this, newServer);
        Simulator.instance().unqueueEntitysNextEvents(this);
        Simulator.instance().removeEntityFromMap(this);

        log.info("{}: {} ({}) reinstalled as task queue.", Simulator.instance().getCurrentTime(),
                this.getClass().getSimpleName(), this.hashCode());

        return newServer;
    }

    @Override
    public long getUptime() {
        return uptime;
    }

    /**
     * @return A DNSServer in state RUNNING if any exists. Otherwise null
     */
    protected DNSServer getDNSServer(){
        return Cloud.getWorkingDNSServer(dnsServerList);
    }

    /**
     * Method to be called when install is finished. Changes server state to RUNNING and setups the server
     */
    @Override
    public void finishReinstall () {
        state = Computer.RUNNING;

        rebuildAfterInstall();
    }

    public abstract void rebuildAfterInstall ();

    /**
     * Method to be called when server crashes.
     * This servers planned events will be removed
     */
    @Override
    public void breakdown() {
        state = Computer.FAILED;

        Simulator.instance().unqueueEntitysNextEvents(this);

        log.info("{}: {} ({}) broke down.", Simulator.instance().getCurrentTime(),
                this.getClass().getSimpleName(), this.hashCode());
    }
}



