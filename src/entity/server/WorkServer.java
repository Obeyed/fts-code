package entity.server;

import entity.Advert;
import entity.DNSServer;
import entity.Computer;
import entity.Task;
import event.FinishedTaskEvent;
import event.ReQueryDNSEvent;
import event.ServerReQueryEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.Random;

/**
 * Server that polls a task from the TaskQueue for virtual servers
 */
public class WorkServer extends Server {
	private static final Logger log = LoggerFactory.getLogger(WorkServer.class);

	private Task currentTask = null;
	private int serverID;
	private boolean isScout;
	long lastAdBoardCheckTime;

	/**
	 * Constructs a WorkServer
	 * @param state State of the server at creation. See Computer for valid status
	 * @param uptime Uptime of the server at creation.
	 * @param dnsServerList List of DNSServers that will be used for contacting other TaskQueues and AdBoards
	 * @param serverID Virtual server id
	 */
	public WorkServer(int state, long uptime, DNSServer[] dnsServerList, int serverID) {
		super(state, uptime, dnsServerList);
		this.serverID = serverID;
		isScout = determineIfScout();
	}

	/**
	 * Tries to retrieve a task from the assigned virtual server's TaskQueue. If any task is available the task is
	 * assigned to the server and a FinishTaskEvent based on the task's length is created (and enqueued). If no task is
	 * available an event is added that will run this function again at a later point in time
	 */
	public void startNewTask() {
		DNSServer dnsServer = this.getDNSServer();

		if (dnsServer != null) {
			TaskQueueServer[] taskQueueServers = dnsServer.getTaskQueues(serverID, this);

			//If TaskQueueServer is null, an error occurred while retrieving it,
			//and this server will most likely be reinstalled to replace it.
			if (taskQueueServers == null) {
				return;
			}

			for (TaskQueueServer taskQueue : taskQueueServers) {
				if (taskQueue.getState() == Computer.RUNNING) {
					Task retrievedTask = taskQueue.getTask();
					if (currentTask == retrievedTask || currentTask == null) {
						currentTask = retrievedTask;
					}
					else {
						log.warn("Inconsistency in task queues.");
					}
				}
			}
		}

		if (currentTask != null) {
			long taskCompletionTime = currentTask.getRemainingRuntime() + Simulator.instance().getCurrentTime();
			FinishedTaskEvent finishedTaskEvent = new FinishedTaskEvent(taskCompletionTime, this);
			Simulator.instance().queueEvent(currentTask, finishedTaskEvent);
		}
		else {
			long nextQueryTime =  (Integer) Simulator.instance().getParams().get(SimulationParameters.REQUERY_INTERVAL_KEY) +
					Simulator.instance().getCurrentTime();
			ServerReQueryEvent newEvent = new ServerReQueryEvent(nextQueryTime, this);
			Simulator.instance().queueEvent(this, newEvent);
		}
	}

	/**
	 * Removes any events related to the current task.
	 * With some likelyhood posts a advert for the assigned virtual server.
	 * With some increasing likelyhood (depending on the last time of reading an advert) reads an advert. If an advert
	 * is read and the WorkServer is a scout the WorkServer changes virtual server. If the WorkServer read an advert
	 * and is not a scout it begins working on the task.
	 *
	 * If no other AdBoard is available an event is added that will try to start a new task.
	 */
	public void finishTask() {
		Simulator.instance().removeEntityFromMap(currentTask);
		currentTask = null;

		DNSServer dnsServer = getDNSServer();

		if (dnsServer != null) {
			AdBoardServer[] adBoardServers = dnsServer.getAdBoards(this);

			//If AdBoardServer is null, an error occurred while retrieving it,
			//and this server will most likely be reinstalled to it.
			if (adBoardServers == null) {
				return;
			}

			Random rand = new Random();
			double advertPostingProbability = (Double)Simulator.instance().getParams()
					.get(SimulationParameters.ADVERT_POSTING_PROBABILITY_KEY);
			boolean postAd = rand.nextDouble() < advertPostingProbability;

			if (postAd){
				long postTime = Simulator.instance().getCurrentTime();
				Advert advert = new Advert(this.serverID, postTime);
				for (AdBoardServer server : adBoardServers) {
					if (server.getState() == Computer.RUNNING) {
						server.postAdvert(advert);
					}
				}
			}

			//Advert reading probability
			//TODO: Refine this use of random to correspond with the bee algorithm.
			boolean willAttemptChange = rand.nextBoolean();

			SimulationParameters params = Simulator.instance().getParams();
			long nextCheckTime = lastAdBoardCheckTime + (Integer)params.get(SimulationParameters.ATTEMPT_CHANGE_INTERVAL_KEY);
			if ((nextCheckTime < Simulator.instance().getCurrentTime()) && willAttemptChange){

				if (isScout) {
					int virtualServerCount = (Integer)Simulator.instance().getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY);
					int newVirtualServer = rand.nextInt(virtualServerCount);
					this.reinstallAsWorker(getUptime(), newVirtualServer);
				}
				else {
					Advert readAdvert = null;
					for (AdBoardServer server : adBoardServers) {
						if (server.getState() == Computer.RUNNING) {
							readAdvert = server.getAdvert();
							break;
						}
					}

					if (readAdvert != null && readAdvert.getVirtualServerId() != serverID){
						lastAdBoardCheckTime = Simulator.instance().getCurrentTime();
						log.info("Reinstalling to server {}.", readAdvert.getVirtualServerId());
						this.reinstallAsWorker(getUptime(), readAdvert.getVirtualServerId());
					}
					else {
						startNewTask();
					}
				}
			}
			else {
				startNewTask();
			}
		}
		else {
			long timeOfRequery = (Integer)Simulator.instance().getParams().get(SimulationParameters.REQUERY_DNS_INTERVAL_KEY)
					+ Simulator.instance().getCurrentTime();
			ServerReQueryEvent event = new ServerReQueryEvent(timeOfRequery, this);
			Simulator.instance().queueEvent(this,event);
		}
	}

	/**
	 * Method to be called when reinstall is finished. Changes server state to RUNNING and tries to start a new task on
	 * the server.
	 */
	@Override
	public void finishReinstall() {
		super.finishReinstall();

		lastAdBoardCheckTime = Simulator.instance().getCurrentTime();

		startNewTask();
	}

	@Override
	public void rebuildAfterInstall() {
		//Do nothing
	}

	/**
	 * Method to be called this server crashes.
	 * This servers planned events will be removed.
	 * The currently assigned task's event wil also be removed.
	 */
	@Override
	public void breakdown() {
		super.breakdown();

		//Remove the completion event for the task.
		Simulator.instance().unqueueEntitysNextEvents(currentTask);
	}

	/**
	 * Determines whether a newly created WorkServer should be a scout.
	 * @return true depending on the probability: virtualServerCount/totalServerCount*scoutsPerVirtualServer.
	 */
	private boolean determineIfScout () {
		Random rand = new Random();
		int virtualServerCount = (Integer)Simulator.instance().getParams().get(SimulationParameters.VIRTUAL_SERVER_COUNT_KEY);
		int totalServerCount = (Integer)Simulator.instance().getParams().get(SimulationParameters.SERVER_COUNT_KEY);

		double percentageOfScouts = Math.ceil(((double)virtualServerCount / totalServerCount) *
				(Double)Simulator.instance().getParams().get(SimulationParameters.SCOUTS_PR_SERVER_KEY));

		return rand.nextDouble() < percentageOfScouts;
	}
}
