package entity.server;

import entity.Cloud;
import entity.DNSServer;
import entity.Computer;
import entity.Task;
import event.ReQueryDNSEvent;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.LinkedList;

/**
 * Server hosting the task queue of a virtual server.
 */
public class TaskQueueServer extends Server {
	private static int MAX_QUEUE_SIZE;
	LinkedList<Task> taskQueue;
	int virtualServerId;

	/**
	 * Constructs a TaskQueueServer with a limited queue size
	 * @param state State of the server at creation. See Computer for valid status
	 * @param uptime Uptime of the server at creation.
	 * @param dnsServerList List of DNSServer that will be used for contacting TaskQueues and Adboards
	 * @param virtualServerId The virtual server the workServer is assigned to
	 */
	public TaskQueueServer(int state, long uptime, DNSServer[] dnsServerList, int virtualServerId) {
		super(state, uptime, dnsServerList);

		MAX_QUEUE_SIZE = (Integer) Simulator.instance().getParams().get(SimulationParameters.MAX_QUEUE_SIZE_KEY);

		taskQueue = new LinkedList<Task>();
		this.virtualServerId = virtualServerId;
	}

	/**
	 * @return the task in the front of the queue
	 */
	public Task getTask () {
		return taskQueue.poll();
	}

	/**
	 * @return a copy of the taskQueue
	 */
	public LinkedList<Task> getTaskQueue () {
		return (LinkedList<Task>) taskQueue.clone();
	}

	public int getVirtualServerId() {
		return virtualServerId;
	}

	/**
	 * Enqueues a task if the queue is not full. Otherwise nothing happens
	 * @param task
	 */
	public void queueTask(Task task) {
		if (!isQueueFull()) {
			taskQueue.offer(task);
		}
	}

	public boolean isQueueFull() {
		return taskQueue.size() >= MAX_QUEUE_SIZE;
	}

	/**
	 * Method to setup tasks after install. If no other TaskServer is running an event is added that will
	 * run this function again at a later point in time
	 */
	@Override
	public void rebuildAfterInstall() {
		DNSServer dnsServer = getDNSServer();

		if (dnsServer != null) {
			TaskQueueServer[] oldServers = dnsServer.getTaskQueues(virtualServerId, null);
			LinkedList<Task> existingTaskQueue = null;

			if (oldServers != null) {
				for (TaskQueueServer server : oldServers) {
					if (server.getState() == Computer.RUNNING) {
						existingTaskQueue = server.getTaskQueue();
						break;
					}
				}
			}

			if (existingTaskQueue != null) {
				taskQueue = existingTaskQueue;
			}
		}
		else {
			long timeOfRequery = (Integer)Simulator.instance().getParams().get(SimulationParameters.REQUERY_DNS_INTERVAL_KEY)
					+ Simulator.instance().getCurrentTime();
			ReQueryDNSEvent event = new ReQueryDNSEvent(timeOfRequery, this);
			Simulator.instance().queueEvent(this,event);
		}
	}
}
