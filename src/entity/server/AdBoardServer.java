package entity.server;

import entity.Advert;
import entity.Cloud;
import entity.DNSServer;
import entity.Computer;
import event.ReQueryDNSEvent;
import event.TearDownAdvertEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simulator.SimulationParameters;
import simulator.Simulator;

import java.util.ArrayList;
import java.util.Random;

/**
 * A server that handles ad board responsibilities.
 */
public class AdBoardServer extends Server {
    /**
     * Logger instance
     */
    private static final Logger log = LoggerFactory.getLogger(AdBoardServer.class);
    /**
     * List of active adverts
     */
	private ArrayList<Advert> advertList;

    /**
     * Constructor for ABoardServer
     * @param state State of the server at creation. See Computer for valid status
     * @param uptime Uptime of the server at creation.
     * @param dnsServerList List of DNSServer that will be used for contacting other TaskQueues and Adboards
     */
    public AdBoardServer(int state, long uptime, DNSServer[] dnsServerList) {
        super(state, uptime, dnsServerList);
        advertList = new ArrayList<Advert>();
    }

    /**
     * Gets a random advert from the set of adverts.
     * If set is empty, system shuts down.
     * Generates random index from 0 to size of set.
     * @return Returns a random advert
     */
	public Advert getAdvert() {
        Advert advert = null;

        if (!advertList.isEmpty()) {
            advert = advertList.get(new Random().nextInt(advertList.size()));
        }

        return advert;
    }

    /**
     * Add advert to set
     * @param advert The advert to be added to the set
     */
	public void postAdvert(Advert advert) {
        advertList.add(advert);

        long expirationTime = (Integer)Simulator.instance().getParams().get(SimulationParameters.ADVERT_EXPIRATION_TIME_KEY)
                + Simulator.instance().getCurrentTime();
        TearDownAdvertEvent newEvent = new TearDownAdvertEvent(expirationTime, this, advert);
        Simulator.instance().queueEvent(advert, newEvent);
	}

    /**
     * Removes an advert from the set
     * @param advert The advert to be removed from the set
     */
	public void tearDownAdvert (Advert advert) {
        if(!advertList.remove(advert)) {
            log.warn("Tried to remove unknown advert from set of adverts.");
        }
        Simulator.instance().removeEntityFromMap(advert);
    }

    /**
     *
     * @return Returns the list of adverts
     */
    public ArrayList<Advert> getAdvertList () {
        return (ArrayList<Advert>) advertList.clone();
    }

    /**
     * Method to be called this server crashes.
     * This servers planned events will be removed
     */
    @Override
    public void breakdown() {
        super.breakdown();

        for (Advert advert : advertList) {
            Simulator.instance().unqueueEntitysNextEvents(advert);
        }
    }

    /**
     * Method to setup adverts after install. If no other Adboard server is running an event is added that will make
     * cause this method to be run again at a later point in time
     */
    @Override
    public void rebuildAfterInstall () {
        DNSServer dnsServer = getDNSServer();

        if (dnsServer != null) {
            AdBoardServer[] oldServers = dnsServer.getAdBoards(null);
            ArrayList<Advert> existingAdvertList = null;

            if (oldServers != null) {
                for (AdBoardServer server : oldServers) {
                    if (server.getState() == Computer.RUNNING) {
                        existingAdvertList = server.getAdvertList();
                        break;
                    }
                }
            }

            if (existingAdvertList != null) {
                advertList = existingAdvertList;
            }
        }
        else {
            long timeOfRequery = (Integer)Simulator.instance().getParams().get(SimulationParameters.REQUERY_DNS_INTERVAL_KEY)
                    + Simulator.instance().getCurrentTime();
            ReQueryDNSEvent event = new ReQueryDNSEvent(timeOfRequery, this);
            Simulator.instance().queueEvent(this,event);
        }
    }
}
